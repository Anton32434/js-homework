Рекурсия это когда функция вызываеться внутри функции (грубо говоря).

Пример:
function recursion(num) {
    if (num === 1) {
        return 1;
    } else {
        return num * (recursion(num - 1));
    }
}
